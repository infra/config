# Pigweed Config

This repository contains configs for Pigweed. The source of truth for these
configs is the corresponding [internal
repository](https://pigweed-internal.git.corp.google.com/infra/config).
